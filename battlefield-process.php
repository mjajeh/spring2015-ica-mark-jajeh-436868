<?php

if(!isset($_POST['ammo']) or !isset($_POST['soldiers']) or !isset($_POST['critique']) or !isset($_POST['duration'])){
	printf ("Error sorry");
	exit;
}

$ammo=(int)$_POST['ammo'];
$soldiers=(int)$_POST['soldiers'];
$critique=(string)$_POST['critique'];
$duration=(float)$_POST['duration'];

	require 'database2.php'; 

			$stmt = $mysqli->prepare("insert into reports(ammunition,soldiers,duration,critique) values (?,?,?,?);");

			$stmt->bind_param('iids', $ammo, $soldiers,$duration,$critique);
 
			$stmt->execute();

			if(!$stmt){
			printf("Query Prep Failed: %s\n", $mysqli->error);
			exit;}


			header("Location: battlefield-submit.html");