<!DOCTYPE html>
<head>
<meta charset="utf-8"/>
<title>Battlefield analysis</title>
<style type="text/css">
body{
	width: 760px; /* how wide to make your web page */
	background-color: teal; /* what color to make the background */
	margin: 0 auto;
	padding: 0;
	font:12px/16px Verdana, sans-serif; /* default font */
}
div#main{
	background-color: #FFF;
	margin: 0;
	padding: 10px;
}

table, th, td {
    border: 1px solid black;
    border-collapse: collapse;
}
th, td {
    padding: 5px;
}
</style>
</head>
<body><div id="main">
<h1> Battlefield Analysis </h1>
<h2> Latest Critiques </h2>
	<p>
<?php

		require 'database2.php'; 
			
			$stmt = $mysqli->prepare("select critique from reports order by posted limit 5;");
						
				$stmt->execute();
				if(!$stmt){
				printf("Query Prep Failed: %s\n", $mysqli->error);
				exit;}

				$result= $stmt->get_result();
				while( $row = $result->fetch_assoc()){
	
					printf("Here are the 5 most recent critiques %s",htmlspecialchars( $row["critique"]));	
					}
?>
	</p>


<h2> Battle Statistics </h2>
<p>
	<?php
		require 'database2.php'; 

			$stmt2 = $mysqli->prepare("select soldiers, soldiers/duration as pounds from reports order by soldiers desc;");
				

				if(!$stmt2){
				printf("Query Prep Failed: %s\n", $mysqli->error);
				exit;}			
				$stmt2->execute();
				$result2= $stmt2->get_result();	

?>
	</p>

	<table style="width:100%">
  <tr>
    <th>Number of Soldiers</th>
    <th>Pounds of Ammunition per Second</th>		

  </tr>
<?php

		while($row2 = $result2->fetch_assoc()){
							//cycle through all the stories
					printf("<tr> <td> %d </td> <td> %f </td> </tr>",htmlspecialchars( $row2["soldiers"]),(float)htmlspecialchars( $row2["pounds"]));	
					}


?>
</table>
<br>
<br>
<br>
<a href="battlefield-submit.html">Submit a New Batttle Report</a>



 
<!-- CONTENT HERE -->
 
</div></body>


</html>